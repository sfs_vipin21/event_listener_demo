<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

use App\Events\LoginHistory;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name'=>'required|max:60',
            'email'=>'required|email|unique:users|max:100',
            'password'=>'required|max:20',
        ]);

        // $validatedData['password'] = bcrypt($request->password);

        $request['password'] = bcrypt($request->password);

        $user = User::create($request->all());

        $accessToken = $user->createToken('authToken')->accessToken;



        return response(['user'=>$user, 'accessToken'=>$accessToken, 'status'=>200]);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if(!auth()->attempt($loginData))
        {
            return response(['message' => 'Invalid Credentials', 'status'=> 401]);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        event(new LoginHistory(auth()->user()));

        return response(['user'=>auth()->user(), 'accessToken'=>$accessToken, 'status'=>200]);
    }

    public function logout()
    {
        if(\Auth::user())
        {
            $user = auth()->user()->token();
            $user->revoke();

            return response(['success'=>'Logged out successfully', 'status'=>200]);
        }
        else
        {
            return response()->jason([
                'failed' => 'Logout Failed',
                'status' => 201
            ]);
        }
    }
}
